package eu.battlecraft.perversionresources;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Logger;

import static org.mockito.ArgumentMatchers.*;
import static org.powermock.api.mockito.PowerMockito.*;

public class BattlecraftTestInstanceCreator {

  private Logger logger;
  private TestLogHandler logHandler;

  private Server mockServer;
  private CommandSender commandSender;

  private static final File pluginDirectory = new File("target/server/plugins/coretest");
  private static final File serverDirectory = new File("target/server");
  private static final File worldsDirectory = new File("target/server");

  private JavaPlugin plugin;

  @SuppressWarnings("deprecation")
  public boolean setUp(String pluginName, String pluginVersion, Class<? extends JavaPlugin> pluginClass) {
    //noinspection ResultOfMethodCallIgnored
    pluginDirectory.mkdirs();

    logger = spy(Logger.getLogger("Test"));
    logHandler = new TestLogHandler();
    logger.addHandler(logHandler);

    mockServer = mock(Server.class);
    JavaPluginLoader mockPluginLoader = mock(JavaPluginLoader.class);
    Whitebox.setInternalState(mockPluginLoader, "server", mockServer);
    when(mockServer.getName()).thenReturn("TestBukkit");
    spy(Logger.getLogger("Minecraft")).setParent(logger);
    when(mockServer.getLogger()).thenReturn(logger);

    PluginDescriptionFile pdf = PowerMockito.spy(new PluginDescriptionFile(pluginName, pluginVersion, pluginClass.getName()));
    when(pdf.getAuthors()).thenReturn(new ArrayList<>());
    try {
      Constructor<? extends JavaPlugin> constructor = pluginClass.getConstructor(JavaPluginLoader.class, PluginDescriptionFile.class, File.class, File.class);
      JavaPlugin jp = constructor.newInstance(mockPluginLoader, pdf, pluginDirectory, new File(pluginDirectory, "testPluginFile"));
      plugin = spy(jp);
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
      e.printStackTrace();
    }

    //doReturn(pluginDirectory).when(plugin).getDataFolder();

    //doReturn(true).when(plugin).isEnabled();
    //doReturn(logger).when(plugin).getLogger();

    // Add Core to the list of loaded plugins
    JavaPlugin[] plugins = new JavaPlugin[]{plugin};

    // Mock the Plugin Manager
    PluginManager mockPluginManager = PowerMockito.mock(PluginManager.class);
    when(mockPluginManager.getPlugins()).thenReturn(plugins);
    when(mockPluginManager.getPlugin(pluginName)).thenReturn(plugin);
    when(mockPluginManager.getPermission(anyString())).thenReturn(null);

    when(mockServer.getPluginManager()).thenReturn(mockPluginManager);

    BukkitScheduler mockScheduler = mock(BukkitScheduler.class);
    when(mockScheduler.scheduleSyncDelayedTask(any(Plugin.class), any(Runnable.class), anyLong())).
        thenAnswer((Answer<Integer>) invocation -> {
          Runnable arg;
          try {
            arg = (Runnable) invocation.getArguments()[1];
          } catch (Exception e) {
            return null;
          }
          arg.run();
          return null;
        });
    when(mockScheduler.scheduleAsyncDelayedTask(any(Plugin.class), any(Runnable.class))).
        thenAnswer((Answer<Integer>) invocation -> {
          Runnable arg;
          try {
            arg = (Runnable) invocation.getArguments()[1];
          } catch (Exception e) {
            return null;
          }
          arg.run();
          return null;
        });
    when(mockScheduler.runTaskLater(any(Plugin.class), any(Runnable.class), anyLong()))
        .thenAnswer((Answer<BukkitTask>) invocation -> {
          Runnable arg;
          try {
            arg = (Runnable) invocation.getArguments()[1];
          } catch (Exception e) {
            return null;
          }
          arg.run();
          BukkitTask task = mock(BukkitTask.class);
          when(task.getTaskId()).thenReturn(1);
          return task;
        });
    when(mockScheduler.runTaskLaterAsynchronously(any(Plugin.class), any(Runnable.class), anyLong()))
        .thenAnswer((Answer<BukkitTask>) invocation -> {
          Runnable arg;
          try {
            arg = (Runnable) invocation.getArguments()[1];
          } catch (Exception e) {
            return null;
          }
          arg.run();
          BukkitTask task = mock(BukkitTask.class);
          when(task.getTaskId()).thenReturn(1);
          return task;
        });
    when(mockServer.getScheduler()).thenReturn(mockScheduler);

    try {
      Field serverfield = JavaPlugin.class.getDeclaredField("server");
      serverfield.setAccessible(true);
      serverfield.set(plugin, mockServer);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }

    Bukkit.setServer(mockServer);

    plugin.onLoad();
    plugin.onEnable();

    return true;
  }

  public boolean tearDown() {

    plugin.onDisable();
    try {
      FileUtils.deleteDirectory(serverDirectory);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return true;
  }

  public Server getMockServer() {
    return mockServer;
  }

  public CommandSender getCommandSender() {
    return commandSender;
  }

  public JavaPlugin getPlugin() {
    return plugin;
  }

  public TestLogHandler getLogHandler() {
    return logHandler;
  }
}

