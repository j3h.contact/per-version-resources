package eu.battlecraft.perversionresources;

import com.viaversion.viaversion.api.Via;
import com.viaversion.viaversion.api.ViaAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Via.class, ViaAPI.class, PluginManager.class, Permission.class, Bukkit.class, PluginDescriptionFile.class, JavaPluginLoader.class})
@PowerMockIgnore("javax.management.*")
public class ListenerTest {

    private static BattlecraftTestInstanceCreator creator;

    @BeforeClass
    public static void setUp() {
        creator = new BattlecraftTestInstanceCreator();
        assertTrue(creator.setUp("PerVersionResources", "1.0", Main.class));
    }

    @AfterClass
    public static void tearDown() {
        creator.tearDown();
    }

    private Listener listener;
    private Main main;
    private UUID uuid;
    private PlayerJoinEvent event;
    private ViaAPI api;
    private Player p;

    @Before
    public void before() {
        main = (Main) creator.getPlugin();
        listener = new Listener(main);

        uuid = UUID.randomUUID();
        p = mock(Player.class);
        when(p.getUniqueId()).thenReturn(uuid);
        when(p.isOnline()).thenReturn(true);
        when(p.isValid()).thenReturn(true);

        event = new PlayerJoinEvent(p, "join");

        mockStatic(Via.class);
        api = mock(ViaAPI.class);
        when(Via.getAPI()).thenReturn(api);
    }

    @Test
    public void testv1_8() {

        when(api.getPlayerVersion(uuid)).thenReturn(ProtocolVersion.v1_8.getVersionNumber());

        listener.onJoin(event);
        verify(p).setResourcePack("https://www.example.com/res1.8.zip");


    }

    @Test
    public void testv1_9() {
        when(api.getPlayerVersion(uuid)).thenReturn(ProtocolVersion.v1_9.getVersionNumber());

        listener.onJoin(event);
        verify(p).setResourcePack("https://www.example.com/res1.9.zip");
    }

    @Test
    public void testv1_11() {
        when(api.getPlayerVersion(uuid)).thenReturn(ProtocolVersion.v1_11.getVersionNumber());

        listener.onJoin(event);
        verify(p).setResourcePack("https://www.example.com/res1.9.zip");
    }

    @Test
    public void testv1_12_2() {
        when(api.getPlayerVersion(uuid)).thenReturn(ProtocolVersion.v1_12_2.getVersionNumber());

        listener.onJoin(event);
        verify(p).setResourcePack("https://www.example.com/res1.12.zip");
    }

    //There is no pack set for very futuristic, so 1.12 pack is expected
    @Test
    public void testv14() {
        when(api.getPlayerVersion(uuid)).thenReturn(999999);

        listener.onJoin(event);
        verify(p).setResourcePack("https://www.example.com/res1.12.zip");
    }

    //Assume 1.15 is set to empty url, make sure no pack is set
    @Test
    public void testv14_without_pck() {
        when(api.getPlayerVersion(uuid)).thenReturn(ProtocolVersion.v1_15.getVersionNumber());
        main.versionToUrl.put(ProtocolVersion.v1_15.getVersionNumber(), "");

        listener.onJoin(event);
        verify(p, times(0)).setResourcePack(anyString());
        main.versionToUrl.remove(ProtocolVersion.v1_15.getVersionNumber());
    }

}
