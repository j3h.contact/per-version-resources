package eu.battlecraft.perversionresources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static org.junit.Assert.fail;

public class TestLogHandler extends Handler {

  private List<LogRecord> list = new ArrayList<>();

  public boolean assertLog(String text, Level level) {
    for (LogRecord logRecord : list) {
      if (logRecord.getLevel().equals(level)) {
        if (logRecord.getMessage().contains(text))
          return true;
      }
    }
    fail("'" + text + "' was not logged at level " + level.getName());
    return false;
  }

  public void clearLog() {
    list.clear();
  }


  @Override
  public void publish(LogRecord record) {
    list.add(record);
  }

  @Override
  public void flush() {

  }

  @Override
  public void close() throws SecurityException {

  }

  public List<LogRecord> getList() {
    return list;
  }
}

