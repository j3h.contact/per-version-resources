package eu.battlecraft.perversionresources;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.logging.Level;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PluginManager.class, Permission.class, Bukkit.class, PluginDescriptionFile.class, JavaPluginLoader.class})
@PowerMockIgnore("javax.management.*")
public class MainTest {

    private static BattlecraftTestInstanceCreator creator;

    @BeforeClass
    public static void setUp() {
        creator = new BattlecraftTestInstanceCreator();
        assertTrue(creator.setUp("PerVersionResources", "1.0", Main.class));
    }

    @AfterClass
    public static void tearDown() {
        creator.tearDown();
    }

    @Test
    public void testDefaultConfig() {
        Main main = (Main) creator.getPlugin();

        assertNotNull(main);
        assertNotNull(main.versionToUrl);
        assertNotNull(main.versionToUrl.get(ProtocolVersion.v1_8.getVersionNumber()));

        assertEquals("https://www.example.com/res1.8.zip", main.versionToUrl.get(ProtocolVersion.v1_8.getVersionNumber()));
        assertEquals("https://www.example.com/res1.9.zip", main.versionToUrl.get(ProtocolVersion.v1_9.getVersionNumber()));
        assertEquals("https://www.example.com/res1.12.zip", main.versionToUrl.get(ProtocolVersion.v1_12_2.getVersionNumber()));

    }

    @Test
    public void testInvalidRelease() {
        Main main = (Main) creator.getPlugin();

        main.getConfig().set("resources.v1_8_10", "https://foo.bar");
        main.saveConfig();

        main.loadVersions();

        creator.getLogHandler().assertLog("Unknown release name defined", Level.WARNING);
    }

    @Test
    public void testUndefinedRelease() {
        Main main = (Main) creator.getPlugin();

        String url = main.getUrl(ProtocolVersion.v1_11.getVersionNumber());

        //1.11 is not defined and should fall back to 1.9
        assertEquals(url, "https://www.example.com/res1.9.zip");

        //1.7 is not defined
        url = main.getUrl(ProtocolVersion.v1_7_8.getVersionNumber());
        assertEquals(url, "");
    }

}
