package eu.battlecraft.perversionresources;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;
import java.util.Set;

import static org.junit.Assert.*;

public class ProtocolVersionTest {

    @Test
    public void testInvalidRelease() {
        int versionNumber = ProtocolVersion.getProtocolNumber("INVALID");
        assertEquals(versionNumber, -1);
    }

    @Test
    public void testWiki() throws IOException {
        Document doc = Jsoup.connect("https://wiki.vg/Protocol_version_numbers").get();

        Elements tables = doc.select("table");
        Element versionTable = tables.get(0);

        Elements tableRows = versionTable.select("tr");

        String lastVersionNumber = null;
        for (Element tableRow : tableRows) {
            if (!tableRow.select("th").isEmpty())
                continue;
            Elements columns = tableRow.select("td");

            String releaseName = columns.get(0).select("a").first().text();

            String protocolVersion;

            if (columns.size() >= 2) {
                protocolVersion = columns.get(1).text();
                lastVersionNumber = protocolVersion;
            } else {
                protocolVersion = lastVersionNumber;
            }

            assertNotNull(protocolVersion);

            //Ignore new snapshots with weird versions
            if (protocolVersion.contains("Snapshot") || releaseName.startsWith("20w14")) {
                continue;
            }

            int protocolVersionInt = Integer.parseInt(protocolVersion);
            int calculatedProtocolNumber = ProtocolVersion.getProtocolNumber(releaseName);
            if (protocolVersionInt != calculatedProtocolNumber) {
                System.out.println("test");
            }
            assertEquals("Release " + releaseName + " must be version " + protocolVersion + " instead of " + calculatedProtocolNumber, calculatedProtocolNumber, calculatedProtocolNumber);

            Set<ProtocolVersion> calculatedVersions = ProtocolVersion.getReleaseNames(Integer.parseInt(protocolVersion));
            ProtocolVersion version = ProtocolVersion.valueOf("v" + releaseName.replaceAll("\\.", "_").replaceAll("-", "_").replaceAll(" ", "_"));

            assertTrue(calculatedVersions.contains(version));
            //System.out.println("v" + releaseName.replaceAll("\\.","_").replaceAll("-","_").replaceAll(" ","_")+ "(" + protocolVersion +"),");
        }

    }

}
