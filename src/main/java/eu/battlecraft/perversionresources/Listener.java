package eu.battlecraft.perversionresources;

import com.viaversion.viaversion.api.Via;
import com.viaversion.viaversion.api.ViaAPI;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Listener implements org.bukkit.event.Listener {

    private Main main;

    Listener(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        ViaAPI api = Via.getAPI();
        int version = api.getPlayerVersion(e.getPlayer().getUniqueId());
        ProtocolVersion protocolVersion = ProtocolVersion.getReleaseNames(version).iterator().next();

        String url = main.getUrl(version);

        if (url.length() > 0) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!e.getPlayer().isOnline())
                        return;

                    if (e.getPlayer().isValid())
                        e.getPlayer().setResourcePack(url);
                }
            }.runTaskLater(main, 20L);

        }

        main.getLogger().info(e.getPlayer().getName() + " joined with " + protocolVersion.getReleaseName() + "(" + protocolVersion.getVersionNumber() + ") and got '" + url + "'");
    }

}
