package eu.battlecraft.perversionresources;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin {

    Map<Integer, String> versionToUrl = new HashMap<>();

    public Main(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
        super(loader, description, dataFolder, file);
    }

    //bukkit default constructor
    public Main() {
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();

        loadVersions();

        this.getServer().getPluginManager().registerEvents(new Listener(this), this);

        new Metrics(this, 12809);
    }

    protected String getUrl(int versionProtocolNumber) {

        while (!versionToUrl.containsKey(versionProtocolNumber) && versionProtocolNumber >= 0) {
            versionProtocolNumber--;
        }

        if (versionProtocolNumber <= -1) {
            return "";
        }

        return versionToUrl.get(versionProtocolNumber);
    }

    protected void loadVersions() {
        this.versionToUrl.clear();

        for (String release : this.getConfig().getConfigurationSection("resources").getKeys(false)) {
            String url = this.getConfig().getString("resources." + release);

            try {
                //Assume protocol version number
                int versionNumber = Integer.parseInt(release);
                versionToUrl.put(versionNumber, url);
            } catch (NumberFormatException e) {
                //Assume release in form v1_xx_xx
                if (release.startsWith("v")) {
                    release = release.substring(1);
                }
                int versionNumber = ProtocolVersion.getProtocolNumber(release);
                if (versionNumber >= 0) {
                    versionToUrl.put(versionNumber, url);
                } else {
                    this.getLogger().warning("Unknown release name defined: " + release + ". See http://wiki.vg/Protocol_version_numbers for valid release names. If your release is newer than " + ProtocolVersion.values()[0].getReleaseName() + " you have to use the protocol version number instead of the release name.");
                }
            }
        }

    }


}
